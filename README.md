# MOVIEBOX
All movies in one app...

## Image Caching
This MOVIEBOX version utilizes Picasso library to handle the movie posters downloading process, which includes an already active caching fuction.

## Rating View 
The rating view custom class was built based upon an Github example that works with this same view approach and also was modified in order to make it look similar to the requested view.

## List Pagination
The most popular movies list utilizes a RecyclerView extension function to handle the pagination when scrolling down.

## 3rd party libraries
* [Picasso](https://square.github.io/picasso/) for image loading.

## Preview
![Playing now and Most popular movies lists](/movieslists.jpg "Playing now and Most popular lists")

![Movie details](/moviedetail.jpg "Movie details")
