package com.backbase.assignment.ui

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.marginEnd
import com.backbase.assignment.R
import com.backbase.assignment.ui.api.EndPoints
import com.backbase.assignment.ui.api.HttpClient
import com.backbase.assignment.ui.util.DatesAndTimes
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_detail.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class MovieDetailActivity : Activity() {

    private lateinit var poster: ImageView
    private lateinit var title: TextView
    private lateinit var releaseDate: TextView
    private lateinit var description: TextView
    private lateinit var genres: LinearLayout
    private lateinit var close: ImageButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        poster = findViewById(R.id.poster)
        title = findViewById(R.id.title)
        releaseDate = findViewById(R.id.releaseDate)
        description = findViewById(R.id.description)
        genres = findViewById(R.id.genres)
        close = findViewById(R.id.close)

        //getting movie id from previous screen
        val movieId = intent.getStringExtra("movieId")

        //setting up close button event
        close.setOnClickListener { finish() }

        fetchMovie(movieId!!)

    }

    private fun fetchMovie(movieId: String) {

        doAsync {

            val movie =  HttpClient.call("${EndPoints.baseUrl}/movie/$movieId?api_key=${EndPoints.yourKey}&language=en-US")

            uiThread {
                Picasso.with(this@MovieDetailActivity).load("https://image.tmdb.org/t/p/original/${movie["poster_path"].asString}").placeholder(R.drawable.ic_baseline_image_24).error(R.drawable.ic_baseline_image_24).fit().into(poster)
                title.text = movie["title"].asString
                val duration = DatesAndTimes.FormatIso8601ToSimpleDate(movie["release_date"].asString).plus(" - ").plus(DatesAndTimes.FormatIso8601ToSimpleTime(movie["runtime"].asString))
                releaseDate.text = duration
                description.text = movie["overview"].asString
                val movieGenres = movie["genres"].asJsonArray

                //creating each movie genre and adding to genre list
                movieGenres.forEach {
                    val genreObject = it.asJsonObject
                    val genre = TextView(this@MovieDetailActivity)
                    genre.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    genre.background = ContextCompat.getDrawable(this@MovieDetailActivity, R.color.colorWhite)
                    genre.setPadding(10, 10, 10, 10)
                    val param = genre.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(0, 0, 10, 0)
                    genre.layoutParams = param
                    genre.setTextColor(resources.getColor(R.color.colorPrimary))
                    genre.textSize = 12F
                    genre.text = genreObject["name"].asString.toUpperCase()
                    genres.addView(genre)
                }

            }
        }

    }
}