package com.backbase.assignment.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.api.EndPoints
import com.backbase.assignment.ui.api.HttpClient
import com.backbase.assignment.ui.movie.MoviesAdapter
import com.backbase.assignment.ui.util.Extensions.addOnScrolledToEnd
import com.backbase.assignment.ui.util.Validator
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class MainActivity : AppCompatActivity() {

    private lateinit var mostPopularMoviesAdapter: MoviesAdapter
    private lateinit var playingNowMoviesAdapter: MoviesAdapter
    private lateinit var playingNowList: RecyclerView
    private lateinit var mostPopularList: RecyclerView
    private lateinit var playingNowHeader: TextView
    private lateinit var mostPopularHeader: TextView
    private var numberPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        playingNowList = findViewById(R.id.playingNowList)
        mostPopularList = findViewById(R.id.mostPopularList)
        playingNowHeader = findViewById(R.id.playingNowHeader)
        mostPopularHeader = findViewById(R.id.mostPopularHeader)


        //setting up playing now adapter
        playingNowMoviesAdapter = MoviesAdapter(listType = 0){ movie: JsonObject ->
            movieClicked(
                movie
            )
        }

        //setting up playing now list
        playingNowList.apply {
            setHasFixedSize(true)
            adapter = playingNowMoviesAdapter
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
        }

        //setting up most popular adapter
        mostPopularMoviesAdapter = MoviesAdapter(listType = 1){ movie: JsonObject ->
            movieClicked(
                movie
            )
        }

        //setting up most popular list
        mostPopularList.apply {
            setHasFixedSize(true)
            adapter = mostPopularMoviesAdapter
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            val itemDecoration = DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
            itemDecoration.setDrawable(ContextCompat.getDrawable(this.context, R.drawable.line_decoration)!!)
            addItemDecoration(itemDecoration)
        }

        //setting up most popular list pagination
        mostPopularList.addOnScrolledToEnd {
            fetchMostPopularMovies()
        }

        //calling out movie api
        fetchPlayingNowMovies()
        fetchMostPopularMovies()
    }


    private fun fetchPlayingNowMovies() {

        doAsync {
            val jsonObjectPoster = HttpClient.call("${EndPoints.baseUrl}/movie/now_playing?language=en-US&page=undefined&api_key=${EndPoints.yourKey}")
            if (Validator.propertyExists(jsonObjectPoster, "results")){
                playingNowMoviesAdapter.items = jsonObjectPoster["results"] as JsonArray
            }
            uiThread {
                playingNowHeader.visibility = View.VISIBLE
                playingNowMoviesAdapter.notifyDataSetChanged()
            }
        }

    }

    private fun fetchMostPopularMovies() {

        doAsync {
            numberPage++
            val jsonObjectFull = HttpClient.call("${EndPoints.baseUrl}/movie/popular?api_key=${EndPoints.yourKey}&language=en-US&page=${numberPage}")
            if (Validator.propertyExists(jsonObjectFull, "results")){
                mostPopularMoviesAdapter.addItems(jsonObjectFull["results"] as JsonArray)
            }
            uiThread {
                mostPopularHeader.visibility = View.VISIBLE
                mostPopularMoviesAdapter.notifyDataSetChanged()
            }
        }

    }

    //setting up movies event click
    private fun movieClicked(movie : JsonObject) {
        val showMovieDetailActivityIntent = Intent(this@MainActivity, MovieDetailActivity::class.java)
        showMovieDetailActivityIntent.putExtra("movieId", movie["id"].asString)
        startActivity(showMovieDetailActivityIntent)
    }

}
