package com.backbase.assignment.ui.movie

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.custom.RatingView
import com.backbase.assignment.ui.util.DatesAndTimes
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.squareup.picasso.Picasso


class MoviesAdapter(var items: JsonArray = JsonArray(), private val listType: Int, private val clickListener: (JsonObject) -> Unit) :
    RecyclerView.Adapter<MoviesAdapter.BaseViewHolder<*>>() {

    companion object {
        private const val TYPE_POSTER = 0
        private const val TYPE_FULL = 1
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when(viewType){
            TYPE_FULL -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.movie_item, parent, false)
                ViewHolderFull(view)
            }
            TYPE_POSTER -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.movie_poster_item, parent, false)
                ViewHolderPoster(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }


    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int){

        val element = items[position]
        when (holder) {
            is ViewHolderFull -> holder.bind(element as JsonObject, clickListener)
            is ViewHolderPoster -> holder.bind(element as JsonObject, clickListener)
            else -> throw IllegalArgumentException()
        }
    }

    fun addItems(newItems: JsonArray){
        items.addAll(newItems)
    }


    override fun getItemCount() = items.size()

    inner class ViewHolderFull(itemView: View) : BaseViewHolder<JsonObject>(itemView) {
        lateinit var poster: ImageView
        lateinit var title: TextView
        lateinit var releaseDate: TextView
        lateinit var runtime: TextView
        lateinit var rating: RatingView

        override fun bind(item: JsonObject, clickListener: (JsonObject) -> Unit) = with(itemView) {
            poster = itemView.findViewById(R.id.poster)

            Picasso.with(itemView.context).load("https://image.tmdb.org/t/p/original/${item["poster_path"].asString}").placeholder(R.drawable.ic_baseline_image_24).error(R.drawable.ic_baseline_image_24).fit().centerCrop().into(poster);

            title = itemView.findViewById(R.id.title)
            title.text = item["title"].asString

            releaseDate = itemView.findViewById(R.id.releaseDate)

            releaseDate.text = DatesAndTimes.FormatIso8601ToSimpleDate(item["release_date"].asString)

            //runtime = itemView.findViewById(R.id.runtime)

            //runtime.text = DatesAndTimes.FormatIso8601ToSimpleTime(item["runtime"].asString)

            rating = itemView.findViewById(R.id.rating)

            val ratingAverage = item["vote_average"].asFloat
            val finalRating = ratingAverage * 10

            rating.setPercentage(finalRating.toInt())

            itemView.setOnClickListener { clickListener(item) }

        }
    }

    inner class ViewHolderPoster(itemView: View) : BaseViewHolder<JsonObject>(itemView) {
        lateinit var poster: ImageView

        override fun bind(item: JsonObject, clickListener: (JsonObject) -> Unit) = with(itemView) {
            poster = itemView.findViewById(R.id.poster)

            Picasso.with(itemView.context).load("https://image.tmdb.org/t/p/original/${item["poster_path"].asString}").placeholder(R.drawable.ic_baseline_image_24).error(R.drawable.ic_baseline_image_24).fit().centerCrop().into(poster);

            itemView.setOnClickListener { clickListener(item) }
        }
    }

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //abstract fun bind(item: T)
        abstract fun bind(item: T, clickListener: (T) -> Unit)
    }

    //setting up type of view
    override fun getItemViewType(position: Int): Int {
        return when (listType) {
            0 -> TYPE_POSTER
            1 -> TYPE_FULL
            else -> throw IllegalArgumentException("Invalid type of data $position")
        }
    }


}