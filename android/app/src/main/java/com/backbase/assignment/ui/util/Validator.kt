package com.backbase.assignment.ui.util

import com.google.gson.JsonObject

object Validator {

    fun propertyExists(jsonObject: JsonObject, property: String): Boolean{
        return jsonObject.has(property)
    }

}