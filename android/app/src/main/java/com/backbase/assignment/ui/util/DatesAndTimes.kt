package com.backbase.assignment.ui.util

import org.joda.time.format.DateTimeFormat

object DatesAndTimes {

    fun FormatIso8601(value: String, pattern: String): String {
        val dateTime = org.joda.time.DateTime(value)
        val dateTimeFormatter = DateTimeFormat.forPattern(pattern)
        return dateTimeFormatter.print(dateTime)
    }

    fun FormatIso8601ToSimpleDate(value: String): String {
        return FormatIso8601(value, "MMMM dd, yyyy")
    }

    fun FormatIso8601ToSimpleTime(value: String): String {
        val runtime = value.toInt()
        val hours = runtime / 60
        val minutes = runtime % 60
        return "${hours}h ${minutes}m"
    }

    fun Now(): org.joda.time.DateTime {
        return org.joda.time.DateTime()
    }

}