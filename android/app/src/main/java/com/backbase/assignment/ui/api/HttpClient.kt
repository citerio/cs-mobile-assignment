package com.backbase.assignment.ui.api

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.net.URL

object HttpClient {

    fun call(url: String): JsonObject{
        val jsonString =
            URL(url).readText()
        return JsonParser.parseString(jsonString).asJsonObject
    }

}