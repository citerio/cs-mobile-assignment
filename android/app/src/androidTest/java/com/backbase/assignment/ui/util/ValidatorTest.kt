package com.backbase.assignment.ui.util

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.google.gson.JsonObject
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ValidatorTest : TestCase() {

    @Test
    fun testPropertyExist(){

        val jsonObject = JsonObject()
        jsonObject.addProperty("iron_man", "tony stark")

        val result = Validator.propertyExists(jsonObject, "iron_man")

        assertThat(result).isTrue()

    }

}